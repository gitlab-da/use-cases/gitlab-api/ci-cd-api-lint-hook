#!/bin/bash

GITLAB_CI_YML=".gitlab-ci.yml"

GITLAB_URL="https://gitlab.com"
GITLAB_CI_LINT_URL="${GITLAB_URL}/api/v4/ci/lint"

GITLAB_CI_YML_CONTENT=$(<$GITLAB_CI_YML)

errors=()
while read -r value; do
	errors+=("$value")
done < <(jq --null-input --arg yaml "${GITLAB_CI_YML_CONTENT}" '.content=$yaml' \
| curl "${GITLAB_CI_LINT_URL}?include_merged_yaml=true" \
--header 'Content-Type: application/json' --data @- --silent \
| jq --raw-output '.errors' | jq -c '.[]')

echo -e "Analysing CI/CD config lint results ..."

count_err=0

for error in "${errors[@]}"; do
	echo "${error}"
	count_err=$count_err+1
done

if [[ $count_err -gt 0 ]]; then
	echo -e "GitLab CI/CD linting errors found. Aborting."
	exit 1
else
	echo -e "GitLab CI/CD linting ok."
	exit 0
fi
