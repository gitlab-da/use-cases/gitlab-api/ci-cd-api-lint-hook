# GitLab CI/CD API Lint Hook

This script can be used as local [Git pre-commit](https://git-scm.com/docs/githooks#_pre_commit) to avoid committing invalid YAML configuration.

![Git Hook example](doc/images/gitlab_ci_cd_api_lint_hook_git_cli.png)

#### Table of Content

[[_TOC_]]

## Introduction

The GitLab API provides the [/ci/lint](https://docs.gitlab.com/ee/api/lint.html#validate-the-ci-yaml-configuration) endpoint which allows to validate a CI/CD YAML configuration, sent as POST request.

> Note:
>
> If you are using [includes](https://docs.gitlab.com/ee/ci/yaml/#include) other than `remote` (local, file, etc.), you need to commit and push the changes into a draft MR to trigger a [full lint](https://docs.gitlab.com/ee/api/lint.html#validate-a-projects-ci-configuration) as an alternative.

## Requirements

- Bash
- curl
- [jq](https://stedolan.github.io/jq/download/)

## Usage

Fork this repository and modify `gitlab-ci-cd-api-lint.sh` with your settings. Git hooks cannot be parametrized.

Test the hook in the local repository:

```
$ cp gitlab-ci-cd-api-lint.sh .git/hooks/pre-commit
```

Copy the script into all environments, or adjust your developer environment provisioning scripts (local, Docker, Gitpod, etc.).

Advanced example: https://gitlab.lrz.de/snippets/234


## Example: Error

Change the `.gitlab-ci.yml` symlink pointing to `.gitlab-ci.error.yml`.

```shell
$ rm .gitlab-ci.yml
$ ln -s .gitlab-ci.error.yml .gitlab-ci.yml
```

Run `gitlab-ci-cd-api-lint.sh`.

```
$ ./gitlab-ci-cd-api-lint.sh
Analysing CI/CD config lint results ...
"jobs test config should implement a script: or a trigger: keyword"
"jobs script config should implement a script: or a trigger: keyword"
"jobs config should contain at least one visible job"
GitLab CI/CD linting errors found. Aborting.
```

Verify the exit code.

```
$ echo $?
1
```

## Example: Working

Change the `.gitlab-ci.yml` symlink pointing to `.gitlab-ci.main.yml`.

```shell
$ rm .gitlab-ci.yml
$ ln -s .gitlab-ci.main.yml .gitlab-ci.yml
```

Run `gitlab-ci-cd-api-lint.sh`.

```
$ ./gitlab-ci-cd-api-lint.sh
Analysing CI/CD config lint results ...
GitLab CI/CD linting ok.
```

Verify the exit code.

```
$ echo $?
0
```

## Development

In case the `.gitlab-ci.yml` file is broken, and the hook prevents you from committing changes,
you can skip the commits using `-n`.

```
$ git add .gitlab-ci.yml
$ git commit -n
```
